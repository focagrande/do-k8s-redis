# How to create primary-replica Redis cluster in Kubernetes

### Introduction

Kubernetes is an open source container orchestration system. It allows you to create, update, and scale containers without worrying about downtime.

In this tutorial, you will create the Redis cluster (not native Redis Cluster) in primary-replica(s) topology in Kubernetes (k8s). We have following expectations:

- We need a Redis primary-replica(s) cluster exposed to the other parts of the infrastructure outside the Kubernetes
- Within Kubernetes cluster primary and replica nodes should have its own permanent DNS names immune from changes in pods/deployments which may eventually lead to changes of internal IP addresses
- From the same reason, Redis nodes should have a possibility to discover each other (especially during `slaveof` operation) using DNS names and not internal IP addresses since they may change during pod/deployment lifecycle.
- We need separate external endpoints for Redis primary and replica nodes. We need to have a possibility of massive write operations to the primary node which should not be disturbed by read operations which in turn should be performed using replicas endpoints.

## Prerequisites

- A basic understanding of Kubernetes objects
- A Kubernetes cluster running on Digital Ocean
- A `kubectl` utility installed and configured on your client machine 

## Step 1. Services for external Redis nodes endpoints

We begin with two services for primary and replica nodes. These are the `LoadBalancer` type of service. Digital Ocean automatically creates LoadBalancers to support these. See YAML definition for primary endpoint:

    apiVersion: v1
    kind: Service
    metadata:
      name: primary
    spec:
      type: LoadBalancer
      ports:
      - protocol: TCP
        port: 6379
        targetPort: 6379
        name: redis
      selector:
        name: redis-primary

In Kubernetes each service has its A DNS record and since we are using the default namespace, our service will have following DNS name:

    primary.default.svc.cluster.local

The `selector` part of service definition will be used to couple service with pod/deployment where actual Redis instances will run.

We create service for replica nodes in similar way:

    apiVersion: v1
    kind: Service
    metadata:
      name: replica
    spec:
      type: LoadBalancer
      ports:
      - protocol: TCP
        port: 6379
        targetPort: 6379
        name: redis
      selector:
        name: redis-replica

We may now take a look at services in our k8s cluster:

    $ kubectl get services
    NAME         TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)          AGE
    kubernetes   ClusterIP      10.245.0.1      <none>           443/TCP          28m
    primary      LoadBalancer   10.245.179.70   46.101.71.184    6379:32080/TCP   117s
    replica      LoadBalancer   10.245.1.40     159.89.215.219   6379:31728/TCP   117s

## Step 2. Deployments with Redis instances

Since we want to have a Redis primary and replica instances up and running independently of pod lifecycle, we create it using deployments. We use predefined Docker images with Redis 3.2 on Alpine Linux. Pay attention to `selector` part of definition since it couples deployment with the service. Another important detail is that replica node uses the DNS name of the primary node to build a Redis cluster topology (not a fixed IP addresses). See YAML definitions below.

Primary:

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: primary-deployment
      labels:
        name: redis-primary
    spec:
      replicas: 1 
      selector:
        matchLabels:
          name: redis-primary
      template:
        metadata:
          labels:
            name: redis-primary
        spec:
          subdomain: primary
          containers:
          - name: redis
            image: redis:3.2.0-alpine
            command:
              - "redis-server"
            args:
              - "--protected-mode"
              - "no"
            ports:
            - containerPort: 6379

Replicas:

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: replica-deployment
      labels:
        name: redis-replica
    spec:
      replicas: 2 
      selector:
        matchLabels:
          name: redis-replica
      template:
        metadata:
          labels:
            name: redis-replica
        spec:
          subdomain: replica
          containers:
          - name: redis
            image: redis:3.2.0-alpine
            command:
              - "redis-server"
            args:
              - "--slaveof"
              - "primary.default.svc.cluster.local"
              - "6379"
              - "--protected-mode"
              - "no"   
            ports:
            - containerPort: 6379

We may now have a peek at k8s deployments:

    $ kubectl get deployments
    NAME                       DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
    redis-primary-deployment   1         1         1            1           4m31s
    redis-replica-deployment   2         2         2            2           4m31s

and pods:

    $ kubectl get pods
    NAME                                        READY   STATUS    RESTARTS   AGE
    redis-primary-deployment-5d7c5c549-lrskl    1/1     Running   0          5m20s
    redis-replica-deployment-697b87d7c8-2xbzc   1/1     Running   0          5m20s
    redis-replica-deployment-697b87d7c8-7vk4q   1/1     Running   0          5m20s

## Step 3. Accessing Redis nodes

We may now use regular Redis console client to operate with Redis instances using load balancer addresses.

Take a look at Redis primary replication info. You may see two replicas connected:

    $ redis-cli -h 46.101.71.184 -p 6379 info replication
    # Replication
    role:master
    connected_slaves:2
    slave0:ip=10.244.84.5,port=6379,state=online,offset=575,lag=1
    slave1:ip=10.244.84.6,port=6379,state=online,offset=575,lag=1
    master_repl_offset:575
    repl_backlog_active:1
    repl_backlog_size:1048576
    repl_backlog_first_byte_offset:2
    repl_backlog_histlen:574

Let's take a look at one of Redis replica replication info. You may may see primary (master) specified using DNS name:

    $ redis-cli -h 159.89.215.219 -p 6379 info replication
    # Replication
    role:slave
    master_host:primary.default.svc.cluster.local
    master_port:6379
    master_link_status:up
    master_last_io_seconds_ago:9
    master_sync_in_progress:0
    slave_repl_offset:673
    slave_priority:100
    slave_read_only:1
    connected_slaves:0
    master_repl_offset:0
    repl_backlog_active:0
    repl_backlog_size:1048576
    repl_backlog_first_byte_offset:0
    repl_backlog_histlen:0

You may check the cluster performance using the `redis-benchmark` utility on the primary node:

    $ redis-benchmark -h 46.101.71.184 -p 6379


## Conclusion

In this tutorial you have created a Redis primary-replica(s) cluster within Kubernetes. Topology of Redis cluster is built using DNS names created for Kubernetes services. Using `LoadBalancer` type of service allows to access Redis primary and replica nodes from outside Kubernetes cluster.

#### Appendix

- Install kubectl

        sudo apt-get update && sudo apt-get install -y apt-transport-https
        curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
        echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
        sudo apt-get update
        sudo apt-get install -y kubectl

- Create a new Kubernetes cluster

        curl -X POST -H "Content-Type: application/json" \
                     -H "Authorization: Bearer your-do-api-key" \
                     -d '{"name": "redis-cluster-poc-01",
                          "region": "fra1",
                          "version": "1.12.1-do.2",
                          "tags": [
                            "redis",
                            "primary-replica"
                          ],
                          "node_pools": [
                            {
                              "size": "s-1vcpu-2gb",
                              "count": 1,
                              "name": "redis-pool"
                            }
                          ]
                        }' \
                     "https://api.digitalocean.com/v2/kubernetes/clusters"

- Get list of Kubernetes clusters (you'll need cluster id in further steps):

        curl -X GET -H "Content-Type: application/json" \
                    -H "Authorization: Bearer your-do-api-key" "https://api.digitalocean.com/v2/kubernetes/clusters"

- Retrieve the kubeconfig for a Kubernetes cluster (store it in `~/.kube/config`)

        curl -X GET -H "Content-Type: application/json" \
                    -H "Authorization: Bearer your-do-api-key" \
                    "https://api.digitalocean.com/v2/kubernetes/clusters/$YOUR_K8S_CLUSTER_ID/kubeconfig"

- Create services and cluster

        kubectl create -f ./do-k8s-redis.yaml

- Delete a Kubernetes cluster

        curl -X DELETE -H "Content-Type: application/json" \
                       -H "Authorization: Bearer your-do-api-key" "https://api.digitalocean.com/v2/kubernetes/clusters/$YOUR_K8S_CLUSTER_ID"
